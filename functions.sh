## functions used by different scripts

function getContainerIp {
    ma=($(echo $(lxc list $containerName -c 4 --format csv) | tr " " "\n"))
    echo ${ma[0]}
}

function waitForNetwork {
    printf "%s\n" "waiting for network"
    while [ -z $(getContainerIp) ]
    do
        printf "."
        sleep 1s
    done
    echo $(getContainerIp)
}
