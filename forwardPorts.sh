#!/bin/bash

source ./kukulkan.conf
source ./functions.sh

##
# Forwards all Ports (except host-ssh) to the container

containerIP=$(getContainerIp)

echo 'FORWARDING ALL PORTS (other than '$hostSshListeningPort') to container IP: ' $containerIP

## PREROUTING is a phase (not a chain)
## for listing PREROUTING rules use
## iptables -L -n -t nat
## --
## FORWARD is a chain
## for listing FORWARD rules use
## iptables -S FORWARD

# ggf vorhandene alte Regeln loeschen
iptables -F
iptables -F -t nat

# definierten Systemstandard wiederherstellen
# ATTENTION: funktioniert noch nicht wie geplant falls der container zwischenzeitlich neugestartet wurde
iptables-restore < $configFileIptablesHostDefault

# if you need anoter port, free it here!
((sshPortPrevious=$hostSshListeningPort-1))
((availablePort=$hostSshListeningPort+1))
((sshPortNext=$hostSshListeningPort+2))

echo "$sshPortPrevious < ssh to host: $hostSshListeningPort < availabe for host service: $availablePort < $sshPortNext"

iptables -A PREROUTING -t nat -i $configNetworkDevice -p tcp --dport 1:$sshPortPrevious -j DNAT --to $containerIP:1-$sshPortPrevious
iptables -A PREROUTING -t nat -i $configNetworkDevice -p tcp --dport $sshPortNext:65535 -j DNAT --to $containerIP:$sshPortNext-65535

iptables -A FORWARD -p tcp -d $containerIP --dport 1:$sshPortPrevious -j ACCEPT
iptables -A FORWARD -p tcp -d $containerIP --dport $sshPortNext:65535 -j ACCEPT
