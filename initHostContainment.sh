#!/bin/bash

source ./kukulkan.conf

## install snap package manager
apt-get update
apt-get install -y snapd

## snap is the recommended way of installation for lxd
# the snap script also takes care of creating a new unprivileged user
snap install lxd --channel=3.0/stable

read -p "Interactively setting up container environment. It is okay to accept all defaults. [Ok]"

# sometimes (e.g. google Cloud) another lxd is pre installed. if not, just ignore
lxd.migrate

# use full path because $PATH might not contain snap folder yet
/snap/bin/lxd init

# saves the current iptables for backup
mkdir -p $configPathIptablesHostDefault
iptables-save > $configFileIptablesHostDefault
