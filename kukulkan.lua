--[[
    Customized chisel
   derivated from spy_users.lua chisel by Draios Inc dba Sysdig
   (original work licensed under the Apache License, Version 2.0)
   --]]

-- Chisel description
description = "Lists every command that users launch interactively (e.g. from bash) and every directory users visit within containers.";
short_description = "Display interactive user activity within containers";
category = "Security";

-- Chisel argument list
args =
{}

require "common"
terminal = require "ansiterminal"

MAX_ANCESTOR_NAVIGATION = 16
max_depth = -1

-- Initialization callback
function on_init()
    
    -- Request the fields needed for this chisel
    fetype = chisel.request_field("evt.type")
    fexe = chisel.request_field("proc.exe")
    fargs = chisel.request_field("proc.args")
    fdir = chisel.request_field("evt.arg.path")
    fuser = chisel.request_field("user.name")
    fdtime = chisel.request_field("evt.time.s")
    fpid = chisel.request_field("proc.pid")
    fppid = chisel.request_field("proc.ppid")
    fcontainername = chisel.request_field("container.name")
    fcontainerid = chisel.request_field("container.id")
    fanames = {}
    fapids = {}
    
    -- set the filter
    chisel.set_filter("(evt.type=execve and evt.dir=<) and container.name!=host")
    
    for j = 0, MAX_ANCESTOR_NAVIGATION do
        fanames[j] = chisel.request_field("proc.aname[" .. j .. "]")
        fapids[j] = chisel.request_field("proc.apid[" .. j .. "]")
    end
    
    sessionName = os.date("%Y%m%d_%H%M%S")    
    sessionFolder = "/root/log/" .. sessionName .. "/"
    
    os.execute("mkdir -p " .. sessionFolder)
    logfile = io.open(sessionFolder .. "kukulkan.log", "a+")
    logfile:setvbuf("no")
    
    os.execute("ln -s -f " .. sessionFolder .. "kukulkan.log /root/kukulkan.log")
    
    print("session started " .. sessionFolder)
    
    return true
end


process_tree = {}

-- Event parsing callback
function on_event()
    
    local user = evt.field(fuser)
    local dtime = evt.field(fdtime)
    local pid = evt.field(fpid)
    local ppid = evt.field(fppid)
    local ischdir = evt.field(fetype) == "chdir"
    local containername = evt.field(fcontainername)
    local containerid = evt.field(fcontainerid)
    local aname
    local icorr = 1
    
    if ischdir then
        ppid = pid
        table.insert(fanames, 0, 0)
        table.insert(fapids, 0, 0)
        icorr = 0
    end
    
    if user == nil then
        user = "<NA>"
    end
    
    if not process_tree[ppid] then
        -- No parent pid in the table yet.
                -- Add one and make sure that there's a shell among the ancestors
        process_tree[ppid] = {-1}
        
        for j = 1, MAX_ANCESTOR_NAVIGATION do
            aname = evt.field(fanames[j])
            
            if aname == nil then
                if evt.field(fapids[j]) == nil then
                    -- no shell in the ancestor list, hide this command
                    break
                end
            elseif string.len(aname) >= 2 and aname:sub(-2) == "sh" then
                apid = evt.field(fapids[j])
                if process_tree[apid] then
                    process_tree[ppid] = {j - 1, apid}
                else
                    process_tree[ppid] = {0, apid}
                end
            end
        end
    end
    
    if process_tree[ppid][1] == -1 then
        -- the parent process has already been detected as NOT having a shell ancestor
        return true
    end
    
    if not process_tree[pid] then
        process_tree[pid] = {1 + process_tree[ppid][1], process_tree[ppid][2]}
    end
    
    if ischdir then
        log(
            extend_string("", 4 * (process_tree[pid][1] - icorr)) .. process_tree[pid][2] .. " " ..
                    dtime .. " " ..
                    user .. "@" ..
                    containername ..") cd " ..
                    evt.field(fdir))
    else
        log(
            extend_string("", 4 * (process_tree[pid][1] - 1)) ..  process_tree[pid][2] ..  " " ..
                    dtime .. " " ..
                    user .. "@" ..
                    containername ..") " ..
                    evt.field(fexe) .. " " ..
                    evt.field(fargs))
    end
    
    return true
end

function log(logstr)
    print(logstr)    
    logfile:write(logstr .. "\n")
end

-- Called by the engine at the end of the capture (Ctrl-C)
function on_capture_end()
    logfile:close()
    print("Session saved: " .. sessionFolder)
end
