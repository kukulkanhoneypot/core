#!/bin/bash

source ./kukulkan.conf
source ./functions.sh

##
# This script initializes the container environment and creates the container 

echo "creating container $containerName (if not yet exists)"
lxc launch images:$containerDistro/$containerDistroVersion $containerName

waitForNetwork

echo "updating guest OS"
lxc exec $containerName -- apt-get update
lxc exec $containerName -- apt-get upgrade -y
