#!/bin/bash

## fills website with data from git

source ./kukulkan.conf

# ubuntu/apache default
htmlFoldername="html/" 

rm -rf $htmlFoldername
mkdir -p $htmlFoldername
git clone $webserverGit $htmlFoldername

# remove .git folder before copying into guest!
rm -rf $htmlFoldername'.git/'

# clean guest
lxc exec $containerName -- rm -rf '/var/www/'$htmlFoldername

echo "copy files to guest"
lxc file push $htmlFoldername $containerName/var/www/ -r
lxc exec $containerName -- chmod 777 /var/www/$htmlFoldername/useruploads/

echo "copied files to guest"
