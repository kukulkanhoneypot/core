#!/bin/bash

source ./kukulkan.conf

## ATTENTION!
# This script changes the port of the ssh-deamon!
# After running this script and after restarting the host and/or ssh-deamon
# any new connection must use the new port!

source ./initHostContainment.sh

source ./initHostSurveillance.sh

source ./initHostSshd.sh

