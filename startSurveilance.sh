#!/bin/bash

source ./kukulkan.conf

echo "download chisel, if new available"
git pull

echo "copy chisel to sysdig chisel folder"
cp ./kukulkan.lua /usr/share/sysdig/chisels/kukulkan.lua

echo 'killing previous screen session, if available'
screen -XS kukulkan quit

echo 'starting surveillance inside screen session "kukulkan"'
screen -d -m -S kukulkan sysdig -c kukulkan

echo 'screen session started. Attach with "screen -r kukulkan". Detach with C-a d'
echo 'see "man screen" for further usage'
