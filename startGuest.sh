#!/bin/bash

source ./kukulkan.conf
source ./functions.sh

## ATTENTION
# This script starts the guest container AND FORWARDS ALL PORTS TO THE GUEST
# Exception is the ssh-host-port which will stay unchanged

# TODO: ignoriere Fehlermeldung, falls nicht vorhanden!
iptables-restore < $configFileIptablesHostDefault

echo "starting guest container $containerName (if not running)"
lxc start $containerName &>/dev/null

waitForNetwork

echo "container $containerName started: $(getContainerIp)"

source ./forwardPorts.sh
