#!/bin/bash

##
# installs sshd inside the container and creates an (dummy) admin-user for server administration
#
# ATTENTION:
# Guest is vulnerable by design!
# this file may intentionally contain bad security practices!

source ./kukulkan.conf

lxc exec $containerName -- apt-get update
lxc exec $containerName -- apt-get install -y openssh-server

echo "(inside container) create account with insecure password $guestAdminName:$guestAdminPassword"
lxc exec $containerName -- adduser --gecos '""' --disabled-password $guestAdminName
echo -e "$guestAdminPassword\n$guestAdminPassword" | lxc exec $containerName -- passwd $guestAdminName

echo "allow admin user to use sudo (usually a good idea)"
lxc exec $containerName -- usermod -aG sudo $guestAdminName

echo "allow admin user to use sudo without password (usually not a good idea)"
tmpfile=$(mktemp)
echo "$guestAdminName ALL=(ALL) NOPASSWD:ALL" > $tmpfile
lxc file push $tmpfile $containerName'/etc/sudoers.d/admin'
