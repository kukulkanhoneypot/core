#!/bin/bash

## (interactively) configures apache inside the container to use https

source ./kukulkan.conf

lxc exec $containerName -- apt-get update
lxc exec $containerName -- apt-get install -y software-properties-common
lxc exec $containerName -- add-apt-repository universe
lxc exec $containerName -- add-apt-repository ppa:certbot/certbot
lxc exec $containerName -- apt-get update

lxc exec $containerName -- apt-get install -y certbot python3-certbot-apache

# interactive script!
# TODO: Standardwerte in .conf hinterlegen
lxc exec $containerName -- certbot --apache
