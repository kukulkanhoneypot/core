#!/bin/bash

source ./kukulkan.conf

# ATTENTION see above
# Change the host-port to the port stated in kukulkan.conf
sed -i -e 's/#Port 22/Port '$hostSshListeningPort'/g' $configFileSSHD

echo "ATTENTION"
echo "HOST-SSH-PORT CHANGED TO $hostSshListeningPort"
echo "Any new connection after restart must use this port!"

service sshd restart
