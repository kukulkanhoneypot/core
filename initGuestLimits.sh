#!/bin/bash

source ./kukulkan.conf

# every containers idmap-range is isolated from every other containers range
lxc config set $containerName security.idmap.isolated true

echo "limit container"
echo "cpu: $containerLimitCpuAllowance"
echo "memory: $containerLimitMemory"
lxc config set $containerName limits.cpu.allowance $containerLimitCpuAllowance
lxc config set $containerName limits.memory $containerLimitMemory

echo "Network: down:$containerLimitNetworkIngress up:$containerLimitNetworkEgress"
lxc config device add $containerName eth0 nic name=eth0 nictype=bridged parent=lxdbr0
lxc config device set $containerName eth0 limits.ingress $containerLimitNetworkIngress
lxc config device set $containerName eth0 limits.egress $containerLimitNetworkEgress
